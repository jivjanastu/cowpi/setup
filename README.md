# COWPi aka Aamne Saamne Pi
Community Owned/Operated Webinar Pi from Janastu

## Install 
After installing the customized RaspiOS image, ensure that the Pi is connected to internet
and then you need to run:
```
bash <(curl -sSL https://gitlab.com/jivjanastu/cowpi/setup/-/raw/master/install.sh)
```
