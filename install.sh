#!/bin/bash

############################################

printf "### Checking internet connectivity ...\n"
if ping www.gitlab.com -c 1 -w 1 > /dev/null; then
   printf "### Connected to internet!!!\n"
else
   printf "### Not connected to internet!!!\n"
   printf "### Please connect to internet and retry\n"
   printf "### Exiting installation...\n"
   exit 1
fi
printf "### --------------------------------- ###\n\n"

############################################

printf "### Updating package lists ...\n"
sudo apt-get -y update
sudo apt-get -y upgrade
printf "### --------------------------------- ###\n\n"

############################################

printf "### Preparing system for Two-Finger Installation\n\n"
sudo apt-get -y install build-essential libx11-dev libxtst-dev libxi-dev x11proto-randr-dev libxrandr-dev xserver-xorg-input-evdev xserver-xorg-input-evdev-dev

############################################

printf "### Preparing system for Yggdrasil installation\n\n"
sudo apt-get -y install dirmngr
gpg --fetch-keys https://neilalexander.s3.dualstack.eu-west-2.amazonaws.com/deb/key.txt
gpg --export 569130E8CA20FBC4CB3FDE555898470A764B32C9 | sudo apt-key add -
echo 'deb http://neilalexander.s3.dualstack.eu-west-2.amazonaws.com/deb/ debian yggdrasil' | sudo tee /etc/apt/sources.list.d/yggdrasil.list

############################################

printf "### Preparing system for Syncthing installation\n\n"
sudo apt-get -y install apt-transport-https
echo "deb https://apt.syncthing.net/ syncthing stable" | sudo tee /etc/apt/sources.list.d/syncthing.list
curl -s https://syncthing.net/release-key.txt | sudo apt-key add -

############################################

printf "### Preparing system for Kolibri Installation\n\n"
sudo apt-get -y install libffi-dev python3-pip python3-pkg-resources
sudo pip3 install pip setuptools --upgrade
sudo pip3 install cffi --upgrade

sudo su -c 'echo "deb http://ppa.launchpad.net/learningequality/kolibri/ubuntu bionic main" > /etc/apt/sources.list.d/learningequality-ubuntu-kolibri-bionic.list'
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys DC5BAA93F9E4AE4F0411F97C74F88ADB3194DD81

############################################

sudo apt-get -y update

############################################

printf "### Installing a list of packages...\n\n"
apps=("git" "nginx" "python3-rpi.gpio" "python3-gpiozero" "php-fpm" "audacity" "yggdrasil" "syncthing" "inkscape" "gimp" "blender" "openshot")
for app in ${apps[@]}; do
    ctr=1
    dpkg -s $app &> /dev/null
    while [ $? -ne 0 ]; do
        echo "### Installing $app. Attempt no. #$ctr"
        sudo apt-get -y install $app
        ctr=$(($ctr+1))
        if ((ctr>3)); then
            echo "Installing $app failed. Please check internet connection and try again"
        exit 1
        fi
	dpkg -s $app &> /dev/null
    done
    echo "$app is Installed!!! ###"
done

############################################

printf "Post install configuration for *** nginx *** \n\n"
sudo sh -c 'cat > /etc/nginx/sites-available/default << END
server {
	listen 80 default_server;
	listen [::]:80 default_server;

	root /var/www/html;

	index index.html index.htm index.nginx-debian.html index.php;

	server_name _;

	location / {
		# First attempt to serve request as file, then
		# as directory, then fall back to displaying a 404.
		try_files \$uri \$uri/ =404;
	}

	
	location ~ \.php$ {
		include snippets/fastcgi-php.conf;
	
		# With php-fpm (or other unix sockets):
		fastcgi_pass unix:/run/php/php7.3-fpm.sock;
		# With php-cgi (or other tcp sockets):
		# fastcgi_pass 127.0.0.1:9000;
	}

}

END'
sudo /etc/init.d/nginx reload
sudo chown -R www-data:pi /var/www/html/
sudo chmod -R 770 /var/www/html/

############################################

printf "Installing and configuring *** home portal *** \n\n"

git clone https://gitlab.com/jivjanastu/cowpi/homeportal.git
mv homeportal $HOME/.homeportal
chmod +x $HOME/.homeportal/syskagaz/gensyskagaz.sh

cat > dhcpcd.enter-hook << END
$HOME/.homeportal/syskagaz/gensyskagaz.sh
END
sudo mv dhcpcd.enter-hook /etc/dhcpcd.enter-hook
sudo cp /etc/dhcpcd.enter-hook /etc/dhcpcd.exit-hook

cat >> $HOME/.everyboot/everyboot.sh << END

bash \$HOME/.homeportal/syskagaz/gensyskagaz.sh

END

sudo sh -c 'cat >> /etc/nginx/sites-available/default << END
server {
	listen 3354;
	listen [::]:3354;

	server_name homeportal;

	root /home/pi/.homeportal;

	index index.html index.htm index.php;

	location / {
		# First attempt to serve request as file, then
		# as directory, then fall back to displaying a 404.
		try_files \$uri \$uri/ =404;
	}

	location ~ \.php$ {
		include snippets/fastcgi-php.conf;
	
		# With php-fpm (or other unix sockets):
		fastcgi_pass unix:/run/php/php7.3-fpm.sock;
		# With php-cgi (or other tcp sockets):
		# fastcgi_pass 127.0.0.1:9000;
	}
}

END'
sudo /etc/init.d/nginx reload

sudo sed -i "s/\%u.*/\%u \-\-app http\:\/\/localhost\:3354/g" /usr/share/raspi-ui-overrides/applications/lxde-x-www-browser.desktop

############################################

printf "Installing and configuring *** two-finger *** \n\n"
git clone https://github.com/Plippo/twofing.git
cd twofing
make
sudo make install
sudo sh -c 'cat >> /usr/share/X11/xorg.conf.d/40-libinput.conf << END

Section "InputClass"
  Identifier "calibration"
  Driver "evdev"
  MatchProduct "WaveShare WS170120"

  Option "EmulateThirdButton" "1"
  Option "EmulateThirdButtonTimeout" "750"
  Option "EmulateThirdButtonMoveThreshold" "30"
EndSection

Section "InputClass"
  Identifier "calibration"
  Driver "evdev"
  MatchProduct "raspberrypi-ts"

  Option "EmulateThirdButton" "1"
  Option "EmulateThirdButtonTimeout" "750"
  Option "EmulateThirdButtonMoveThreshold" "30"
EndSection

END'

cd ..

sudo rm -r twofing

cat >> $HOME/.everyboot/everyboot.sh << END

for event in \$(ls /dev/input/ | grep event)
do
        twofing --wait /dev/input/\$event
done

END

############################################

printf "Post install configuration for *** yggdrasil *** \n\n"
yggdrasil -normaliseconf -useconffile /etc/yggdrasil.conf -json > $HOME/ygg.conf
python3 << END
import json
import socket
import subprocess

with open("/home/pi/ygg.conf","r+") as f:
    data = json.load(f)
    data['Peers']=['tcp://pantoto.net:1194']
    data['NodeInfo']={'hesaru':socket.gethostname(),
                      'desha':subprocess.check_output("wpa_cli -i wlan0 get country", shell=True).decode('utf-8')
                      }
    
    f.seek(0)
    json.dump(data,f)
    f.truncate()
END

sudo sh -c 'yggdrasil -normaliseconf -useconffile /home/pi/ygg.conf > /etc/yggdrasil.conf'
rm $HOME/ygg.conf 

sudo systemctl enable yggdrasil
sudo systemctl start yggdrasil

############################################

#printf "Post install configuration for *** syncthing *** \n\n"
#syncthing -no-browser
#sleep 2

sudo reboot

